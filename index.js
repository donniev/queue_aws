/**
 * module queue
 *
 */
"use strict";
var Promise = require("bluebird"),
	nconf = require('nconf'),
	AWS = require('aws-sdk');

class Queue {
	constructor(options) {
		options = options || {};
		nconf.env();
		this.queueName=options.queueName ||"unknown";
		// passed in parameters override nconf variables
		this.sendQueueUrl = options.urlQueueSend || nconf.get('URL_QUEUE_SEND');
		this.receiveQueueUrl = options.urlQueueReceive || nconf.get('URL_QUEUE_RECEIVE');
		// config AWS
		AWS.config.update({
			accessKeyId    : options.awsAccessKeyId || nconf.get('AWS_ACCESS_KEY_ID'),
			secretAccessKey: options.awsSecretAccessKey || nconf.get('AWS_SECRET_ACCESS_KEY'),
			region         : options.awsRegion || nconf.get('AWS_REGION')
		});
		// create sqs instance which will use configuration above
		this.sqs = new AWS.SQS();

	}

//
// store a message on the sendQueue
//
	initializeQueue() {
		let self=this;
		return new Promise((resolve, reject)=> {
			//we need to check to make sure our queue exists
			this.sqs.listQueues({},(err,data)=>{
				if(err){
					reject(err);
				}else{
					let found=false;
					if(data && data.QueueUrls) {//may be no queues defined
						data.QueueUrls.forEach((q)=> {
							if (q.endsWith("/" + self.queueName)) {
								self.receiveQueueUrl = q;
								found = true;
								resolve(q);
							}
						});
					}
					if(!found){
						self.createQueue(self.queueName,{})
						.then((u)=>{
							self.receiveQueueUrl=u;
							resolve(u)
						})
						.catch(reject)
					}
				}
			});

		})
	}
	sendMessage(message) {
		return new Promise((resolve, reject)=> {
			var params = {
				MessageBody: message,
				QueueUrl   : this.sendQueueUrl
			};
			this.sqs.sendMessage(params, function (err, data) {
				if (err) {
					reject(err)
				} else {
					resolve(data);
				}
			});
		})
	}

//
// read available messages from the receiveQueue
//
	readMessage() {
		return new Promise((resolve, reject)=> {
			var params = {
				QueueUrl           : this.receiveQueueUrl,
				AttributeNames     : [
					'All',
				],
				MaxNumberOfMessages: 10
			};
			this.sqs.receiveMessage(params, (err, data)=> {
				if (err) {
					reject(err);
				} else {
					resolve(data.Messages || []);
				}
			});
		})
	}

//
// remove a message from the receiveQueue
//
	removeMessage(message) {
		return new Promise((resolve, reject)=> {
			var params = {
				QueueUrl     : this.receiveQueueUrl,
				ReceiptHandle: message.ReceiptHandle
			};
			this.sqs.deleteMessage(params, (err, data)=> {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			});
		})
	}
   deleteQueue(url){
	   return new Promise((resolve,reject)=>{
		   this.sqs.deleteQueue({QueueUrl:url},(err,data)=>{
			   if(err){
				   reject(err)
			   }else{
				   resolve(data)
			   }
		   })
	   })
   }
	createQueue(name, attributes) {
		return new Promise((resolve,reject)=> {
			attributes = attributes || {};
			attributes.QueueName=name;
			this.sqs.createQueue(attributes,(err,data)=>{
				if(err){
					reject(err);
				}else{
					resolve(data.QueueUrl)
				}
			})
		});

	}
}
module.exports = Queue;