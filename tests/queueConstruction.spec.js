/**
 * @module queueConstruction.spec
 *
 **/

'use strict';

describe("Check instantion",()=>{
	let Q=require("../index");
	let q =void(0);
	beforeEach(()=>{

		q=new Q({urlQueueSend:"foo"});
	})
	it("check methods",(done)=>{
		expect(typeof q.sendMessage).toBe('function');
		expect(typeof q.readMessage).toBe('function');
		expect(typeof q.removeMessage).toBe('function');
		expect(typeof q.createQueue).toBe('function');
		expect(typeof q.deleteQueue).toBe('function');
		done();
	});it("check constructs",(done)=>{
		expect(q instanceof Q).toBe(true);
		done();
	});
	it('check credentials',(done)=>{
     expect(q.sendQueueUrl).toBe("foo");
		done();
	})
	it("check AWS",(done)=>{
		expect(typeof q.sqs.listQueues).toBe('function');
		done();
	});
	it("check AWS queues",(done)=>{
		q.sqs.listQueues({QueueNamePrefix:"iv"},(err,data)=>{
			expect(err).toBe(null);
			expect(data.QueueUrls.length).toBe(1);
			expect(data.QueueUrls[0].indexOf("iv_queue")).not.toBe(-1);
			done();
		})
	});
	it("create Queue and delete Queue",(done)=> {
		q.createQueue("createdByTest", {})
				.then((results)=> {
					expect(results.indexOf("createdByTest")).not.toBe(-1);
					q.deleteQueue(results)
							.then((res)=> {
								expect(res).not.toBe(null);
								done();
							})
							.catch((err)=> {
								expect(err).toBe(null);
								done();
							});
				})
				.catch((err)=> {
					expect(err).toBe(null);
					done();
				});
	});

})
